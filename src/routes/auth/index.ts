import { Router } from 'express';
import { authenticate, segments, validator } from '@helpers/middlewares';
import { SignInDto, SignUpDto } from '@dto/auth';
import { check, signIn, signOut, signUp, swCheck, swSignIn, swSignOut, swSignUp } from './controllers';

export const swAuthRouter = {
	'/auth/signup': {
		post: { ...swSignUp },
	},
	'/auth/signin': {
		post: { ...swSignIn },
	},
	'/auth/signout': {
		post: { ...swSignOut },
	},
	'/auth/check': {
		get: { ...swCheck },
	},
};

const router = Router();

router.post('/signup', [validator({ [segments.body]: { classType: SignUpDto } })], signUp);
router.post('/signin', [validator({ [segments.body]: { classType: SignInDto } })], signIn);
router.post('/signout', [authenticate], signOut);
router.get('/check', [authenticate], check);

export { router as auth };
