import { Response } from 'express';
import { schema } from '@dto/jsonSchema';
import { errorResponseSchema } from '@helpers/commonSchemas';
import { AuthService } from '@services';
import { TypedRequest } from '@customTypes/utils';
import { SignInDto } from '@dto/auth';

export const swSignIn = {
	tags: ['Authentication'],
	summary: 'Sign In',
	requestBody: {
		content: {
			'application/json': {
				schema: {
					...schema.SignInDto,
				},
			},
		},
	},
	responses: {
		'200': {
			content: {
				'application/json': {
					schema: {
						type: 'object',
						properties: {
							name: {
								type: 'string',
							},
						},
					},
				},
			},
		},
		'400': {
			content: {
				'application/json': {
					schema: {
						...errorResponseSchema,
					},
				},
			},
		},
	},
};

export const signIn = async (req: TypedRequest<SignInDto>, res: Response) => {
	try {
		const { id, name } = await AuthService.signIn(req.body);
		req.session.user = { id };
		res.status(200).json({ name });
	} catch ({ message }) {
		res.status(400).json({ message });
	}
};
