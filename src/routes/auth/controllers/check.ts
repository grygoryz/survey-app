import { Request, Response } from 'express';
import { errorResponseSchema } from '@helpers/commonSchemas';
import { AuthService } from '@services';

export const swCheck = {
	tags: ['Authentication'],
	summary: 'Check for auth state',
	responses: {
		'200': {
			content: {
				'application/json': {
					schema: {
						type: 'object',
						properties: {
							name: {
								type: 'string',
							},
						},
					},
				},
			},
		},
		'400': {
			content: {
				'application/json': {
					schema: {
						...errorResponseSchema,
					},
				},
			},
		},
	},
};

export const check = async (req: Request, res: Response) => {
	try {
		const { id } = req.session.user;
		const { name } = await AuthService.check(id);
		res.status(200).json({ name });
	} catch ({ message }) {
		res.status(400).json({ message });
	}
};
