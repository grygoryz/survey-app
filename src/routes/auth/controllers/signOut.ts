import { Request, Response } from 'express';
import { errorResponseSchema } from '@helpers/commonSchemas';
import { promisify } from 'util';

export const swSignOut = {
	tags: ['Authentication'],
	summary: 'Sign out',
	responses: {
		'204': {},
		'400': {
			content: {
				'application/json': {
					schema: {
						...errorResponseSchema,
					},
				},
			},
		},
	},
};

export const signOut = async (req: Request, res: Response) => {
	try {
		await promisify(req.session.destroy).call(req.session);
		res.sendStatus(204);
	} catch ({ message }) {
		res.status(400).json({ message });
	}
};
