import { Response } from 'express';
import { AuthService } from '@services';
import { schema } from '@dto/jsonSchema';
import { TypedRequest } from '@customTypes/utils';
import { SignUpDto } from '@dto/auth';
import { errorResponseSchema } from '@helpers/commonSchemas';

export const swSignUp = {
	tags: ['Authentication'],
	summary: 'Sign out',
	requestBody: {
		content: {
			'application/json': {
				schema: {
					...schema.SignUpDto,
				},
			},
		},
	},
	responses: {
		'200': {},
		'400': {
			content: {
				'application/json': {
					schema: {
						...errorResponseSchema,
					},
				},
			},
		},
	},
};

export const signUp = async (req: TypedRequest<SignUpDto>, res: Response) => {
	try {
		await AuthService.signUp(req.body);
		res.sendStatus(200);
	} catch ({ message }) {
		res.status(400).json({ message });
	}
};
