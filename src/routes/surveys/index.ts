import { Router } from 'express';
import { authenticate, segments, validator } from '@helpers/middlewares';
import { GetSurveyDto, SubmitSurveyBodyDto, SubmitSurveyDto, SubmitSurveyParamsDto } from '@dto/surveys';
import { submitSurvey, getSurvey, swSubmitSurvey, swGetSurvey } from './controllers';

export const swSurveysRouter = {
	'/surveys/:surveyId': {
		get: { ...swGetSurvey },
		post: { ...swSubmitSurvey },
	},
};

const router = Router();

router.get('/:surveyId', [authenticate, validator({ [segments.params]: { classType: GetSurveyDto } })], getSurvey);
router.post(
	'/:surveyId',
	[
		authenticate,
		validator({
			[segments.body]: { classType: SubmitSurveyBodyDto },
			[segments.params]: { classType: SubmitSurveyParamsDto },
		}),
	],
	submitSurvey
);

export { router as surveys };
