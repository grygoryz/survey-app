import { Response } from 'express';
import { errorResponseSchema } from '@helpers/commonSchemas';
import { TypedRequest } from '@customTypes/utils';
import { GetSurveyDto } from '@dto/surveys';
import { SurveyService } from '@services';
import { questionTypes } from '@entities/question';

export const swGetSurvey = {
	tags: ['Surveys'],
	summary: 'Get survey by id',
	parameters: [
		{
			name: 'surveyId',
			in: 'path',
			required: true,
			schema: {
				type: 'integer',
			},
		},
	],
	responses: {
		'200': {
			content: {
				'application/json': {
					schema: {
						type: 'object',
						properties: {
							title: {
								type: 'string',
							},
							questions: {
								type: 'array',
								items: {
									type: 'object',
									properties: {
										id: {
											type: 'integer',
										},
										title: {
											type: 'string',
										},
										type: {
											type: 'string',
											enum: questionTypes,
										},
										options: {
											type: 'array',
											items: {
												type: 'object',
												properties: {
													id: {
														type: 'integer',
													},
													title: {
														type: 'string',
													},
												},
											},
										},
									},
								},
							},
						},
					},
				},
			},
		},
		'400': {
			content: {
				'application/json': {
					schema: {
						...errorResponseSchema,
					},
				},
			},
		},
	},
};

export const getSurvey = async (req: TypedRequest<{}, {}, GetSurveyDto>, res: Response) => {
	try {
		const survey = await SurveyService.getSurvey(req.params.surveyId);
		res.status(200).json(survey);
	} catch ({ message }) {
		res.status(400).json({ message });
	}
};
