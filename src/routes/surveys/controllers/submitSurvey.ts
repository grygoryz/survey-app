import { Response } from 'express';
import { errorResponseSchema } from '@helpers/commonSchemas';
import { TypedRequest } from '@customTypes/utils';
import { SubmitSurveyBodyDto, SubmitSurveyParamsDto } from '@dto/surveys';
import { SurveyService } from '@services';
import { schema } from '@dto/jsonSchema';

export const swSubmitSurvey = {
	tags: ['Surveys'],
	summary: 'Submit survey',
	requestBody: {
		content: {
			'application/json': {
				schema: {
					...schema.SubmitSurveyBodyDto,
				},
			},
		},
	},
	responses: {
		'200': {},
		'400': {
			content: {
				'application/json': {
					schema: {
						...errorResponseSchema,
					},
				},
			},
		},
	},
};

export const submitSurvey = async (req: TypedRequest<SubmitSurveyBodyDto, {}, SubmitSurveyParamsDto>, res: Response) => {
	try {
		await SurveyService.submitSurvey(req.session.user.id, { surveyId: req.params.surveyId, answers: req.body.answers });
		res.sendStatus(200);
	} catch ({ message }) {
		res.status(400).json({ message });
	}
};
