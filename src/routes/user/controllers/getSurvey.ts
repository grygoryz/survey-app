import { Response } from 'express';
import { errorResponseSchema } from '@helpers/commonSchemas';
import { TypedRequest } from '@customTypes/utils';
import { GetSurveyDto } from '@dto/user';
import { SurveyService } from '@services';
import { questionTypes } from '@entities/question';

export const swGetSurvey = {
	tags: ['User'],
	summary: 'Get survey stats by id',
	parameters: [
		{
			name: 'surveyId',
			in: 'path',
			required: true,
			schema: {
				type: 'integer',
			},
		},
	],
	responses: {
		'200': {
			content: {
				'application/json': {
					schema: {
						type: 'object',
						properties: {
							title: {
								type: 'string',
							},
							totalCompleted: {
								type: 'integer',
							},
							createdAt: {
								type: 'string',
								format: 'date-time',
							},
							questions: {
								type: 'array',
								items: {
									type: 'object',
									properties: {
										id: {
											type: 'integer',
										},
										title: {
											type: 'string',
										},
										type: {
											type: 'string',
											enum: questionTypes,
										},
										options: {
											type: 'array',
											items: {
												type: 'object',
												properties: {
													id: {
														type: 'integer',
													},
													title: {
														type: 'string',
													},
													selectedTimes: {
														type: 'number',
													},
												},
											},
										},
									},
								},
							},
						},
					},
				},
			},
		},
		'400': {
			content: {
				'application/json': {
					schema: {
						...errorResponseSchema,
					},
				},
			},
		},
	},
};

export const getSurvey = async (req: TypedRequest<{}, {}, GetSurveyDto>, res: Response) => {
	try {
		const result = await SurveyService.getSurveyStats(req.params.surveyId);
		res.status(200).json(result);
	} catch ({ message }) {
		res.status(400).json({ message });
	}
};
