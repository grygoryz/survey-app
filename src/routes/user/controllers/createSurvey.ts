import { Response } from 'express';
import { errorResponseSchema } from '@helpers/commonSchemas';
import { TypedRequest } from '@customTypes/utils';
import { CreateSurveyDto } from '@dto/user';
import { schema } from '@dto/jsonSchema';
import { SurveyService } from '@services';

export const swCreateSurvey = {
	tags: ['User'],
	summary: 'Create survey',
	requestBody: {
		content: {
			'application/json': {
				schema: {
					...schema.CreateSurveyDto,
				},
			},
		},
	},
	responses: {
		'201': {
			content: {
				'application/json': {
					schema: {
						type: 'object',
						properties: {
							id: {
								type: 'number',
							},
						},
					},
				},
			},
		},
		'400': {
			content: {
				'application/json': {
					schema: {
						...errorResponseSchema,
					},
				},
			},
		},
	},
};

export const createSurvey = async (req: TypedRequest<CreateSurveyDto>, res: Response) => {
	try {
		const id = await SurveyService.createSurvey(req.session.user.id, req.body);
		res.status(201).json({ id });
	} catch ({ message }) {
		res.status(400).json({ message });
	}
};
