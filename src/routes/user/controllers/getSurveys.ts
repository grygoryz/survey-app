import { Response } from 'express';
import { errorResponseSchema } from '@helpers/commonSchemas';
import { TypedRequest } from '@customTypes/utils';
import { GetSurveysDto, sortOptions } from '@dto/user';
import { SurveyService } from '@services';

export const swGetSurveys = {
	tags: ['User'],
	summary: 'Get created surveys list',
	parameters: [
		{
			name: 'page',
			in: 'query',
			required: true,
			description: 'Page number. Starts from 0.',
			schema: {
				type: 'integer',
			},
		},
		{
			name: 'size',
			in: 'query',
			required: true,
			description: 'Page size',
			schema: {
				type: 'integer',
			},
		},
		{
			name: 'sortDate',
			in: 'query',
			schema: {
				type: 'string',
				enum: sortOptions,
				default: 'DESC',
			},
		},
		{
			name: 'search',
			in: 'query',
			description: 'Search substring',
			schema: {
				type: 'string',
			},
		},
	],
	responses: {
		'200': {
			content: {
				'application/json': {
					schema: {
						type: 'object',
						properties: {
							data: {
								type: 'array',
								items: {
									type: 'object',
									properties: {
										id: {
											type: 'integer',
										},
										title: {
											type: 'string',
										},
										createdAt: {
											type: 'string',
											format: 'date-time',
										},
									},
								},
							},
							navigation: {
								type: 'object',
								properties: {
									count: {
										type: 'number',
									},
									page: {
										type: 'number',
									},
									size: {
										type: 'number',
									},
								},
							},
						},
					},
				},
			},
		},
		'400': {
			content: {
				'application/json': {
					schema: {
						...errorResponseSchema,
					},
				},
			},
		},
	},
};

export const getSurveys = async (req: TypedRequest<{}, GetSurveysDto>, res: Response) => {
	try {
		const { data, count } = await SurveyService.getSurveys(req.session.user.id, req.query);
		res.status(200).json({ data, navigation: { count, page: req.query.page, size: req.query.size } });
	} catch ({ message }) {
		res.status(400).json({ message });
	}
};
