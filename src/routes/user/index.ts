import { Router } from 'express';
import { authenticate, segments, validator } from '@helpers/middlewares';
import { CreateSurveyDto, GetSurveyDto, GetSurveysDto } from '@dto/user';
import { createSurvey, getSurvey, getSurveys, swCreateSurvey, swGetSurvey, swGetSurveys } from './controllers';

export const swUserRouter = {
	'/user/surveys': {
		get: { ...swGetSurveys },
		post: { ...swCreateSurvey },
	},
	'/user/surveys/:surveyId': {
		get: { ...swGetSurvey },
	},
};

const router = Router();

router.get('/surveys', [authenticate, validator({ [segments.query]: { classType: GetSurveysDto } })], getSurveys);
router.post('/surveys', [authenticate, validator({ [segments.body]: { classType: CreateSurveyDto } })], createSurvey);
router.get('/surveys/:surveyId', [authenticate, validator({ [segments.params]: { classType: GetSurveyDto } })], getSurvey);

export { router as user };
