import debug from 'debug';
import { createServer } from 'http';
import { createTerminus } from '@godaddy/terminus';
import { client as redisClient } from '@db/redis';
import { getConnection } from 'typeorm';
import { app } from './server';

import '@db/postgres';

const dg = debug('server');
const { PORT } = process.env;

const httpServer = createServer(app);

httpServer.listen(PORT, () => {
	dg(`Server listens on port ${PORT}`);
});

createTerminus(httpServer, {
	signals: ['SIGINT', 'SIGTERM'],
	onSignal: () => {
		dg('Cleanup started');
		return Promise.all([redisClient.quit(), getConnection().close()]);
	},
	onShutdown: async () => {
		dg('Cleanup finished, server is down');
	},
});
