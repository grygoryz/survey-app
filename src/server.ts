import express, { Request, Response, NextFunction } from 'express';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import { session, cacheControl, rateLimiter } from '@helpers/middlewares';
import { AppError } from '@helpers/errors';
import swaggerUi from 'swagger-ui-express';
import morgan from 'morgan';
import { auth, user, surveys } from '@routes';
import swaggerDocument from './swagger';

const app = express();

// middlewares
app.use(morgan('tiny'));
app.use(
	cors({
		origin: '*',
		credentials: true,
	})
);
app.use(rateLimiter);
app.use(cacheControl);
app.use(session);
app.use(cookieParser());
app.use(bodyParser.json({ limit: '10kb' }));

// routes
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use('/auth', auth);
app.use('/user', user);
app.use('/surveys', surveys);

app.use('*', (req, res, next) => {
	const error = new Error(`Can not find right route for method ${req.method} and path ${req.originalUrl}`);
	next(error);
});

// error handling middleware
app.use((err: AppError, req: Request, res: Response, next: NextFunction) => {
	res.status(err.statusCode || 500).json({ message: err.message });
});

export { app };
