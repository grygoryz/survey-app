import { Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { QuestionOption } from '@entities/questionOption';

@Entity()
export class Answer {
	@PrimaryGeneratedColumn()
	id!: number;

	@ManyToOne(() => QuestionOption, questionOption => questionOption.answers, { onDelete: 'CASCADE' })
	@JoinColumn({ name: 'question_option_id' })
	questionOption!: QuestionOption;
}
