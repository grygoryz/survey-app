import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Survey } from '@entities/survey';

@Entity()
export class User {
	@PrimaryGeneratedColumn()
	id!: number;

	@Column('varchar', { length: 255 })
	name!: string;

	@Column('varchar', { length: 255, unique: true })
	email!: string;

	@Column('varchar', { length: 255, select: false })
	password!: string;

	@OneToMany(() => Survey, survey => survey.user)
	surveys!: Array<Survey>;
}
