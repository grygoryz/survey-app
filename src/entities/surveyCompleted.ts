import { CreateDateColumn, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { User } from '@entities/user';
import { Survey } from '@entities/survey';

@Entity()
export class SurveyCompleted {
	@ManyToOne(() => User, { onDelete: 'CASCADE', primary: true })
	@JoinColumn({ name: 'user_id' })
	user!: User;

	@ManyToOne(() => Survey, { onDelete: 'CASCADE', primary: true })
	@JoinColumn({ name: 'survey_id' })
	survey!: Survey;

	@CreateDateColumn({ name: 'completed_at', type: 'timestamptz' })
	completedAt!: Date;
}
