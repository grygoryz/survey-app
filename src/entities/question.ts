import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Survey } from '@entities/survey';
import { QuestionOption } from '@entities/questionOption';

export const questionTypes = ['select', 'checkbox'] as const;
export type QuestionType = typeof questionTypes[number];

@Entity()
export class Question {
	@PrimaryGeneratedColumn()
	id!: number;

	@Column('varchar', { length: 255 })
	title!: string;

	@Column('smallint')
	order!: number;

	@Column({ type: 'enum', enum: questionTypes, name: 'type' })
	type!: QuestionType;

	@ManyToOne(() => Survey, survey => survey.questions, { onDelete: 'CASCADE' })
	@JoinColumn({ name: 'survey_id' })
	survey!: Survey;

	@OneToMany(() => QuestionOption, questionOption => questionOption.question)
	options!: Array<QuestionOption>;
}
