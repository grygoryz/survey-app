import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Question } from '@entities/question';
import { Answer } from '@entities/answer';

@Entity()
export class QuestionOption {
	@PrimaryGeneratedColumn()
	id!: number;

	@Column('varchar', { length: 255 })
	title!: string;

	@Column('smallint')
	order!: number;

	@ManyToOne(() => Question, question => question.options, { onDelete: 'CASCADE' })
	@JoinColumn({ name: 'question_id' })
	question!: Question;

	@OneToMany(() => Answer, answer => answer.questionOption)
	answers!: Array<Answer>;
}
