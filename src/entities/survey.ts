import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { User } from '@entities/user';
import { Question } from '@entities/question';

@Entity()
export class Survey {
	@PrimaryGeneratedColumn()
	id!: number;

	@Column('varchar', { length: 255 })
	title!: string;

	@CreateDateColumn({ name: 'created_at', type: 'timestamptz' })
	createdAt!: Date;

	@ManyToOne(() => User, user => user.surveys, { onDelete: 'CASCADE' })
	@JoinColumn({ name: 'user_id' })
	user!: User;

	@OneToMany(() => Question, question => question.survey)
	questions!: Array<Question>;
}
