import { createConnection } from 'typeorm';
import connectionOptions from '@db/postgres/config';
import debug from 'debug';

const dg = debug('postgres');

createConnection(connectionOptions)
	.then(() => {
		dg('Connection established');
	})
	.catch((err: Error) => {
		dg(`Connection failed with message: ${err.message}`);
	});
