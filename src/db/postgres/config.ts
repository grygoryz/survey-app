import { ConnectionOptions } from 'typeorm';

const { POSTGRES_HOST, POSTGRES_PORT, POSTGRES_USER, POSTGRES_PASSWORD, POSTGRES_DB, NODE_ENV } = process.env;

const connectionOptions: ConnectionOptions = {
	type: 'postgres',
	host: POSTGRES_HOST,
	port: +(POSTGRES_PORT as string),
	username: POSTGRES_USER,
	password: POSTGRES_PASSWORD,
	database: POSTGRES_DB,
	synchronize: true,
	entities: ['src/entities/*.ts'],
	maxQueryExecutionTime: 1000,
	logging: NODE_ENV !== 'production',
};

export default connectionOptions;
