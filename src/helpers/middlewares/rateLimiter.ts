import { client as redisClient } from '@db/redis';
import { RateLimiterRedis } from 'rate-limiter-flexible';
import { AppError } from '@helpers/errors';
import { ExpressMiddleware } from '../../customTypes';

const rateLimiterRedis = new RateLimiterRedis({
	storeClient: redisClient,
	keyPrefix: 'express_limiter',
	points: 10,
	duration: 1,
});

export const rateLimiter: ExpressMiddleware = async (req, res, next) => {
	try {
		await rateLimiterRedis.consume(req.ip);
		next();
	} catch {
		next(new AppError('Too many requests', 429));
	}
};
