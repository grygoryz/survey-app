import { ExpressMiddleware } from '@customTypes';
import { ClassType, transformAndValidate, TransformValidationOptions } from 'class-transformer-validator';
import { AppError } from '@helpers/errors';
import { ValidationError } from 'class-validator';

export const segments = {
	body: 'body',
	query: 'query',
	params: 'params',
} as const;

interface SegmentOptions {
	classType: ClassType<any>;
	config?: TransformValidationOptions;
}

interface Options extends Partial<Record<keyof typeof segments, SegmentOptions>> {}

export const validator = (options: Options): ExpressMiddleware => async (req, res, next) => {
	const result: Array<ValidationError | null> = await Promise.all(
		Object.keys(options).map(async segment => {
			const segmentValue = segments[segment as keyof typeof segments];
			const { classType, config } = options[segment as keyof Options] as SegmentOptions;
			try {
				req[segmentValue] = await transformAndValidate(classType, req[segmentValue], config);
				return null;
			} catch (validationErrors) {
				return validationErrors;
			}
		})
	);

	const errors = result.flat(1).filter(Boolean) as Array<ValidationError>;
	if (errors.length) {
		const message = errors.map(err => err.toString(false, true)).join('');
		next(new AppError(message, 400));
	} else {
		next();
	}
};
