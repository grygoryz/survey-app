import { ExpressMiddleware } from '../../customTypes';
import { AppError } from '../errors';

export const authenticate: ExpressMiddleware = (req, res, next) => {
	if (!req.session.user) {
		return next(new AppError('Cookie not found', 401));
	}

	next();
};
