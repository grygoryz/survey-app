import { NextFunction, Request, Response } from 'express';
import { Session } from 'express-session';

export interface ICookiePayload {
	id: number;
}

export type ExpressMiddleware = (req: Request, res: Response, next: NextFunction) => void;

declare module 'http' {
	interface IncomingMessage {
		session: Session;
	}
}

declare module 'express-session' {
	interface Session {
		user: ICookiePayload;
	}
}
