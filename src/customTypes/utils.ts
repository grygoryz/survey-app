import { Request } from 'express';
import { ParsedQs } from 'qs';

export type TypedRequest<Body = any, Query = ParsedQs, Params = Record<string, string>> = Request<
	Record<string, string> & Params,
	{},
	Body,
	Query
>;
