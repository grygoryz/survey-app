import { swAuthRouter, swSurveysRouter, swUserRouter } from '@routes';
import { schema } from '@dto/jsonSchema';

const { PORT } = process.env;

const swagger = {
	openapi: '3.0.0',
	info: {
		title: 'Survey API',
		version: '1.0.0',
	},
	servers: [
		{
			url: `http://localhost:${PORT}`,
		},
	],
	paths: {
		...swAuthRouter,
		...swUserRouter,
		...swSurveysRouter,
	},
	definitions: {
		...schema,
	},
};

export default swagger;
