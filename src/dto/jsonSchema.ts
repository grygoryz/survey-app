import { validationMetadatasToSchemas } from 'class-validator-jsonschema';
// @ts-ignore
import { defaultMetadataStorage } from 'class-transformer/cjs/storage';

// import all metadata
import './auth';
import './user';
import './surveys';

export const schema = validationMetadatasToSchemas({
	classTransformerMetadataStorage: defaultMetadataStorage,
});
