import { IsIn, IsInt, IsOptional, Max, MaxLength, Min } from 'class-validator';
import { Type } from 'class-transformer';

export const sortOptions = ['ASC', 'DESC'] as const;
type SortOption = typeof sortOptions[number];

export class GetSurveysDto {
	@Min(0)
	@IsInt()
	@Type(() => Number)
	page!: number;

	@IsInt()
	@Min(1)
	@Max(30)
	@Type(() => Number)
	size!: number;

	@IsOptional()
	@IsIn(sortOptions)
	sortDate!: SortOption | undefined;

	@IsOptional()
	@MaxLength(255)
	search!: string | undefined;
}
