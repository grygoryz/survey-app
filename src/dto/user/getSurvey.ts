import { IsInt } from 'class-validator';
import { Type } from 'class-transformer';

export class GetSurveyDto {
	@IsInt()
	@Type(() => Number)
	surveyId!: number;
}
