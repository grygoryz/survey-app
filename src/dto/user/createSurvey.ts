import { ArrayNotEmpty, IsIn, MaxLength, ValidateNested } from 'class-validator';
import { QuestionType, questionTypes } from '@entities/question';
import { Type } from 'class-transformer';

class Option {
	@MaxLength(255)
	title!: string;
}

class Question {
	@MaxLength(255)
	title!: string;

	@IsIn(questionTypes)
	type!: QuestionType;

	@ArrayNotEmpty()
	@ValidateNested({ each: true })
	@Type(() => Option)
	options!: Array<Option>;
}

export class CreateSurveyDto {
	@MaxLength(255)
	title!: string;

	@ArrayNotEmpty()
	@ValidateNested({ each: true })
	@Type(() => Question)
	questions!: Array<Question>;
}
