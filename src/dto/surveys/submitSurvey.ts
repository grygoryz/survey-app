import { ArrayNotEmpty, ArrayUnique, IsDefined, IsInt, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { applyMixins } from '@helpers/utils';

class Answer {
	@IsInt()
	questionId!: number;

	@ArrayNotEmpty()
	@ArrayUnique()
	@IsInt({ each: true })
	options!: Array<number>;
}

export class SubmitSurveyBodyDto {
	@IsDefined()
	@ArrayNotEmpty()
	@ValidateNested({ each: true })
	@Type(() => Answer)
	answers!: Array<Answer>;
}

export class SubmitSurveyParamsDto {
	@IsInt()
	@Type(() => Number)
	surveyId!: number;
}

export interface SubmitSurveyDto extends SubmitSurveyParamsDto, SubmitSurveyBodyDto {}
export class SubmitSurveyDto {}
applyMixins(SubmitSurveyDto, [SubmitSurveyBodyDto, SubmitSurveyParamsDto]);
