import { IsEmail, MaxLength } from 'class-validator';

export class SignInDto {
	@IsEmail()
	email!: string;

	@MaxLength(255)
	password!: string;
}
