import { IsEmail, MaxLength } from 'class-validator';

export class SignUpDto {
	@IsEmail()
	email!: string;

	@MaxLength(255)
	name!: string;

	@MaxLength(255)
	password!: string;
}
