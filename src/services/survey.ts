import { CreateSurveyDto, GetSurveysDto } from '@dto/user';
import { getCustomRepository, getManager, ILike } from 'typeorm';
import { SurveyRepository } from '@repositories/survey';
import { QuestionRepository } from '@repositories/question';
import { QuestionOptionRepository } from '@repositories/questionOption';
import { Survey } from '@entities/survey';
import { SubmitSurveyDto } from '@dto/surveys';
import { Question } from '@entities/question';
import { QuestionOption } from '@entities/questionOption';
import { AnswerRepository } from '@repositories/answer';
import { SurveyCompletedRepository } from '@repositories/surveyCompleted';

export class SurveyService {
	public static async createSurvey(userId: number, { title, questions }: CreateSurveyDto): Promise<number> {
		return getManager().transaction(async transactionalEntityManager => {
			const surveyRepository = transactionalEntityManager.getCustomRepository(SurveyRepository);
			const questionRepository = transactionalEntityManager.getCustomRepository(QuestionRepository);
			const questionOptionRepository = transactionalEntityManager.getCustomRepository(QuestionOptionRepository);

			const surveyResult = await surveyRepository.insert({ title, user: { id: userId } });
			const surveyId: number = surveyResult.identifiers[0].id;

			const questionsValues = questions.map(({ title: questionTitle, type }, questionIdx) => ({
				type,
				title: questionTitle,
				order: questionIdx,
				survey: { id: surveyId },
			}));
			const questionResult = await questionRepository.insert(questionsValues);

			const optionsValues = questions.flatMap(({ options }, questionIdx) =>
				options.map(({ title: optionTitle }, optionIdx) => ({
					title: optionTitle,
					order: optionIdx,
					question: { id: questionResult.identifiers[questionIdx].id },
				}))
			);
			await questionOptionRepository.insert(optionsValues);

			return surveyId;
		});
	}

	public static async getSurveys(
		userId: number,
		{ page, size, sortDate, search }: GetSurveysDto
	): Promise<{ data: Array<Pick<Survey, 'id' | 'title' | 'createdAt'>>; count: number }> {
		const surveyRepository = getCustomRepository(SurveyRepository);

		const [data, count] = await surveyRepository.findAndCount({
			select: ['id', 'title', 'createdAt'],
			where: { user: { id: userId }, ...(search && { title: ILike(`${search}%`) }) },
			order: {
				createdAt: sortDate || 'DESC',
			},
			skip: page,
			take: size,
		});

		return { data, count };
	}

	public static async getSurvey(
		surveyId: number
	): Promise<
		Pick<Survey, 'title'> & {
			questions: Array<Pick<Question, 'id' | 'title' | 'type'> & { options: Array<Pick<QuestionOption, 'title'>> }>;
		}
	> {
		const surveyRepository = getCustomRepository(SurveyRepository);

		return surveyRepository
			.createQueryBuilder('survey')
			.select(['survey.title', 'question.id', 'question.title', 'question.type', 'option.id', 'option.title'])
			.innerJoin('survey.questions', 'question')
			.innerJoin('question.options', 'option')
			.where('survey.id = :surveyId', { surveyId })
			.orderBy('question.order', 'ASC')
			.addOrderBy('option.order', 'ASC')
			.getOneOrFail();
	}

	public static async getSurveyStats(
		surveyId: number
	): Promise<
		Pick<Survey, 'title' | 'createdAt'> & {
			totalCompleted: number;
			questions: Array<
				Pick<Question, 'id' | 'title' | 'type'> & {
					options: Array<Pick<QuestionOption, 'id' | 'title'> & { selectedTimes: number }>;
				}
			>;
		}
	> {
		return getManager().transaction(async transactionalEntityManager => {
			const surveyRepository = transactionalEntityManager.getCustomRepository(SurveyRepository);
			const surveyCompletedRepository = transactionalEntityManager.getCustomRepository(SurveyCompletedRepository);
			const answerRepository = transactionalEntityManager.getCustomRepository(AnswerRepository);

			const surveyQb = await surveyRepository
				.createQueryBuilder('survey')
				.select([
					'survey.title',
					'survey.createdAt',
					'question.id',
					'question.title',
					'question.type',
					'option.id',
					'option.title',
				])
				.innerJoin('survey.questions', 'question')
				.innerJoin('question.options', 'option')
				.where('survey.id = :surveyId', { surveyId })
				.orderBy('question.order', 'ASC')
				.addOrderBy('option.order', 'ASC');

			const [totalCompleted, survey] = await Promise.all([
				surveyCompletedRepository.getSurveyTotalCompletedCount(surveyId),
				Promise.resolve().then(async () => {
					const surveyResult = await surveyQb.getOneOrFail();
					const questionOptionsIds = surveyResult.questions.flatMap(({ options }) => options.map(option => option.id));
					const countPerOption = await answerRepository.getSelectedCountPerOption(questionOptionsIds);

					return {
						...surveyResult,
						questions: surveyResult.questions.map(question => ({
							...question,
							options: question.options.map(option => ({
								...option,
								selectedTimes: countPerOption[option.id.toString()] || 0,
							})),
						})),
					};
				}),
			]);

			return {
				...survey,
				totalCompleted,
			};
		});
	}

	public static async submitSurvey(userId: number, { surveyId, answers }: SubmitSurveyDto) {
		return getManager().transaction(async transactionalEntityManager => {
			const questionRepository = transactionalEntityManager.getCustomRepository(QuestionRepository);
			const answerRepository = transactionalEntityManager.getCustomRepository(AnswerRepository);
			const surveyCompletedRepository = transactionalEntityManager.getCustomRepository(SurveyCompletedRepository);

			await surveyCompletedRepository.insert({ survey: { id: surveyId }, user: { id: userId } });

			const questions: Array<
				Pick<Question, 'id' | 'type'> & { options: Array<Pick<QuestionOption, 'id'>> }
			> = await questionRepository
				.createQueryBuilder('question')
				.select(['question.id', 'question.type', 'option.id'])
				.innerJoin('question.options', 'option')
				.where('question.survey_id = :surveyId', { surveyId })
				.getMany();

			if (questions.length !== answers.length) {
				throw new Error("Submitted answers length is not equal to survey's questions length");
			}

			questions.forEach(({ id: questionId, type, options }) => {
				const questionAnswer = answers.find(({ questionId: answerId }) => answerId === questionId);
				if (!questionAnswer) {
					throw new Error(`Answer is not provided for question ${questionId}`);
				}
				if (questionAnswer.options.length > 1 && type === 'select') {
					throw new Error(`Only one option can be provided for question ${questionId}`);
				}
				const existingOptionsIds = options.map(option => option.id);
				if (questionAnswer.options.some(optionId => !existingOptionsIds.includes(optionId))) {
					throw new Error(`Not existing optionId provided for question ${questionId}`);
				}
			});

			await answerRepository.insert(
				answers.flatMap(answer => answer.options.map(optionId => ({ questionOption: { id: optionId } })))
			);
		});
	}
}
