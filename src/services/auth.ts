import { getCustomRepository } from 'typeorm';
import { UserRepository } from '@repositories/user';
import bcrypt from 'bcrypt';
import { SignInDto, SignUpDto } from '@dto/auth';
import { User } from '@entities/user';

export class AuthService {
	public static async signUp(data: SignUpDto): Promise<void> {
		const userRepository = getCustomRepository(UserRepository);
		await userRepository.signUp(data);
	}

	public static async signIn({ email, password }: SignInDto): Promise<Pick<User, 'name' | 'id'>> {
		const userRepository = getCustomRepository(UserRepository);
		const user = await userRepository.findOne({ select: ['name', 'password', 'id'], where: { email } });
		if (!user) {
			throw new Error('Credentials are not valid');
		}

		const valid = await bcrypt.compare(password, user.password);
		if (!valid) {
			throw new Error('Credentials are not valid');
		}

		return {
			id: user.id,
			name: user.name,
		};
	}

	public static async check(id: number): Promise<Pick<User, 'name'>> {
		const userRepository = getCustomRepository(UserRepository);
		const user = await userRepository.findOne({ select: ['name'], where: { id } });
		if (!user) {
			throw new Error('User not found');
		}

		return user;
	}
}
