import { EntityRepository, Repository } from 'typeorm';
import { QuestionOption } from '@entities/questionOption';

@EntityRepository(QuestionOption)
export class QuestionOptionRepository extends Repository<QuestionOption> {}
