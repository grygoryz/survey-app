import { EntityRepository, Repository } from 'typeorm';
import { User } from '@entities/user';
import bcrypt from 'bcrypt';

@EntityRepository(User)
export class UserRepository extends Repository<User> {
	async signUp({ name, email, password }: Pick<User, 'name' | 'email' | 'password'>): Promise<void> {
		const passwordHash = await bcrypt.hash(password, 11);
		await this.save({ email, name, password: passwordHash });
	}
}
