import { EntityRepository, Repository } from 'typeorm';
import { Answer } from '@entities/answer';

@EntityRepository(Answer)
export class AnswerRepository extends Repository<Answer> {
	async getSelectedCountPerOption(questionOptionsIds: Array<number>): Promise<Record<string, number>> {
		const resultArray: Array<{ id: number; count: number }> = await this.createQueryBuilder('answer')
			.select(['answer.question_option_id AS id', 'COUNT(answer.question_option_id)::INT AS count'])
			.where('answer.question_option_id IN (:...options)', { options: questionOptionsIds })
			.groupBy('answer.question_option_id')
			.getRawMany();

		return resultArray.reduce<Record<string, number>>((res, next) => ({ ...res, [next.id]: next.count }), {});
	}
}
