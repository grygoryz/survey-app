import { EntityRepository, Repository } from 'typeorm';
import { SurveyCompleted } from '@entities/surveyCompleted';

@EntityRepository(SurveyCompleted)
export class SurveyCompletedRepository extends Repository<SurveyCompleted> {
	async getSurveyTotalCompletedCount(surveyId: number): Promise<number> {
		const result = await this.createQueryBuilder('survey_completed')
			.select('COUNT(survey_completed.survey_id)::INT AS count')
			.where('survey_completed.survey_id = :surveyId', { surveyId })
			.groupBy('survey_completed.survey_id')
			.getRawOne();

		return result?.count || 0;
	}
}
