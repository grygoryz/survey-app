import { EntityRepository, Repository } from 'typeorm';
import { Survey } from '@entities/survey';

@EntityRepository(Survey)
export class SurveyRepository extends Repository<Survey> {}
